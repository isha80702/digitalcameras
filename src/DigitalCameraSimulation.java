public class DigitalCameraSimulation //Main Class created called DigitalCameraSimulation
{
	public static void main(String[] args) {
	PointAndShoot P = new PointAndShoot("Canon", "PowerShotA50",8.0,0,16); //instantiate the PointAndShoot Class
	String result = P.describeCamera(); // call and return of value from describeCamera method
	System.out.println(result);//print values
	PhoneCamera PP = new PhoneCamera("Apple", "IPhone",6.0,64,0); //instantiate the PhoneCamera Class
	String result1 = PP.describeCamera(); / call and return of value from describeCamera method
	System.out.println(result1); // print values
	}
}