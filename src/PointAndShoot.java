//Implementation of Class PointAndShoot
class PointAndShoot extends DigitalCamera {
   
   PointAndShoot(String make, String model, double megapixels, int internalsize, int externalsize)
   {
    this.make = make;
	this.model = model;
	this.megapixels = megapixels;
	this.internalsize = internalsize; 
	this.externalsize = externalsize; 
   }
   
  public  String describeCamera() {
    String details;
    details = make +"\n"+ model + "\nMeagapixels= "+megapixels + "\nExternal Memory Size = "+ externalsize+"GB\n";
    return details;
    }
    
}