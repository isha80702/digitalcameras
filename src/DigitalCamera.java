// Implementation of Class DigitalCamera
 abstract class DigitalCamera {
    String make;
    String model;
    double megapixels;
    int internalsize;
    int externalsize;
    DigitalCamera()
    {
    this.make = " ";
	this.model = " ";
	this.megapixels = 0.0;
	this.internalsize = 0; 
	this.externalsize = 0;  
    }
   abstract String describeCamera();
}