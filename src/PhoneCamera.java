// Implementation of Class PhoneCamera
class PhoneCamera extends DigitalCamera {
    
    PhoneCamera(String make, String model, double megapixels, int internalsize, int externalsize)
    {
    this.make = make;
	this.model = model;
	this.megapixels = megapixels;
	this.internalsize = internalsize; 
	this.externalsize = externalsize; 
    }
    public String  describeCamera() {
     String details;
      details = make +"\n"+ model + "\nMeagapixels= "+megapixels + "\nInternal Memory Size = "+ internalsize +"GB\n";
   return details;
    }
}